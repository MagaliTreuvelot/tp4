/*3 - Afficher les ingrédients des pizzas au survol de leurs noms*/
$(function(){

$(".pizza-type label").hover(
    function() {
      $(this).find(".description").css("display", "inline-block");
}, function() {
      $(this).find(".description").css("display", "none");
});
  })

/*4 - Afficher visuellement le nombre de parts et le nombre de pizzas approprié en dessous du champs « Nombre de parts »*/
$(function(){
    
    $(".nb-parts input").on('input',function() {
        

    var input = $(".nb-parts input").val();
        
    var nb_input = parseInt(input);
        
    var nb_part = nb_input%6;
        
    //Si moins de 7 parts 
     if (nb_input<=6)   
   {
       	$("span.pizza-6").remove();

        switch(nb_input)
            {
                case 0:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-0 pizza-pict"></span>');
                    break;
                }   
                case 1:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-1 pizza-pict"></span>');
                    break;
                }
                case 2:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-2 pizza-pict"></span>');
                    break;

                }
                case 3:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-3 pizza-pict"></span>');
                    break;

                }
                case 4:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-4 pizza-pict"></span>');
                    break;

                }
                case 5:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-5 pizza-pict"></span>');
                    break;

                }
                case 6:
                {
                    $("div.nb-parts").children().last().replaceWith('<span class="pizza-6 pizza-pict"></span>');
                    break;

                }
            }

    
   }
        else if (nb_input>6)
    {
        $("div.nb-parts").children().last().removeClass();

        $("span.pizza-6").remove();

        while(nb_input>0)
        {
            
            $(".nb-parts input").next().before("<span class='pizza-6 pizza-pict'></span>");

            nb_input-=6;

        }
        
        if(nb_part!=0)
            {
               $("span.pizza-6").last().remove();
            }
        
        switch(nb_part){

			 case 1:
                $("div.nb-parts").children().last().replaceWith('<span class="pizza-1 pizza-pict"></span>');
				break;

             case 2:
			    $("div.nb-parts").children().last().replaceWith('<span class="pizza-2 pizza-pict"></span>');
			    break;

			 case 3:
			    $("div.nb-parts").children().last().replaceWith('<span class="pizza-3 pizza-pict"></span>');
			    break;

			 case 4:
                $("div.nb-parts").children().last().replaceWith('<span class="pizza-4 pizza-pict"></span>');
				break;

             case 5:
			    $("div.nb-parts").children().last().replaceWith('<span class="pizza-5 pizza-pict"></span>');
			    break;

			 case 6:
			    $("div.nb-parts").children().last().replaceWith('<span class="pizza-6 pizza-pict"></span>');
			    break;
			    }
    }
                
    
});
})


//5 - Afficher le formulaire de saisie d'adresse au clic sur le bouton "Etape suivante" puis masquer ce même bouton

$(function(){
    
    $("button.btn-success.next-step").click(function(){
        
      $(".infos-client").css("display", "block");
      $(this).css("display","none");
        
    });   
})

//6 - Ajouter une ligne de champ d'adresse lorsque l'on clique sur le bouton "Ajouter un autre ligne d'adresse"
$(function(){
    
    $("button.btn-default").click(function(){
        $('<br><input type="text"/>').insertBefore(".infos-client div.type button.btn-default");


    });   
})

//7 - Au clic sur le bouton de validation, supprimer tous les éléments de la page, et afficher un message de remerciement (Merci PRENOM ! Votre commande sera livrée dans 15 minutes).

$(function(){
    
    $("button.btn-success.done").click(function(){

        var prenom = $("input").eq(-3).val();
        $( ".row.typography-row" ).before( "<p>Merci "+prenom+" ! Votre commande sera livrée dans 15 minutes.</p>" );
        $("div.row").not("input:eq(-3)").remove();
          
    });   
})


/*8 - Actualiser le total de la commande en fonction des éléments choisis grâce à l'attribut data-price. Mettre ce calcul dans une fonction (DRY - Don’t Repeat Yourself)*/
$(function(){
    
    $('input').change(function(){

    var total=0;
    var prix=0;
        
    $(".type.pizza-type input[name=type]:checked").each(function() {
        prix = parseInt($(this).attr('data-price'));
        total += prix;
        });
    
    
    $(".type input[name=pate]:checked").each(function() {
        prix = parseInt($(this).attr('data-price'));
        total += prix;
        });
    
    
    $("input[type=checkbox]").each(function() {
        if(this.checked) {
            prix = parseInt($(this).attr('data-price'));
            total += prix;
        }
        else{
            prix = 0;
        }   
        });
        
        $("div.stick-right p").text(total + " €");

    });
})










